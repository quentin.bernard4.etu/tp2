export default class Component {
	tagName;
	attribute = undefined;
	children = undefined;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}
	render() {
		if (this.children == undefined && this.attribute != undefined) {
			return `<${this.tagName} ${this.attribute.name}=${this.attribute.value} />`;
		}
		return this.renderChildren();
	}
	renderChildren() {
		if (this.children instanceof Array) {
			return `<${this.tagName}>${this.children.join('')}</${this.tagName}>`;
		}
		return `<${this.tagName}>${this.children}</${this.tagName}>`;
	}
}
