import Component from './Components.js';

export default class Img extends Component {
	constructor(children) {
		super('img', {
			name: 'src',
			value: children,
		});
	}
}
